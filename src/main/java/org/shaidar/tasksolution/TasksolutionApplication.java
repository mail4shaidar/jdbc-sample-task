package org.shaidar.tasksolution;

import org.shaidar.tasksolution.db.services.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories
public class TasksolutionApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(TasksolutionApplication.class, args);
        final UserService service = (UserService) applicationContext.getBean("userService");

        // мало нам тестов, запихаем еще в мейн, да?
        // созданием и дропом таблиц занимается ORM, вручную мы этого делать не станем ни тут, ни в сервисах
        service.saveUser("Джо", "Байден", (byte) 78);
        service.saveUser("Трамп", "Дональд", (byte) 74);
        service.saveUser("Барак", "Обама", (byte) 59);
        service.saveUser("Джордж", "Буш", (byte) 74);
        service.removeUserById(2);
        service.getAllUsers();
        service.cleanUsersTable();
    }
}
