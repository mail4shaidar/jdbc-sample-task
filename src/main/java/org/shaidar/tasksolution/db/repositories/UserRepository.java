package org.shaidar.tasksolution.db.repositories;

import org.shaidar.tasksolution.db.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
}
