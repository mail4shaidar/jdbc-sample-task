package org.shaidar.tasksolution.db.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.shaidar.tasksolution.db.entities.UserEntity;
import org.shaidar.tasksolution.db.repositories.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository repository;

    public List<UserEntity> getAllUsers() {
        final List<UserEntity> userEntities = repository.findAll();
        log.info("Получены все пользователи числом {} человек!", userEntities.size());
        return userEntities;
    }

    @Transactional
    public void removeUserById(Integer id) {
        repository.deleteById(id);
        log.info("Удален пользователь {}!", id);
    }

    @Transactional
    public void cleanUsersTable() {
        repository.deleteAll();
        log.info("Удалены все пользователи!");
    }

    @Transactional
    public UserEntity saveUser(String name, String lastName, byte age) {
        final UserEntity entity = new UserEntity();
        entity.setName(name);
        entity.setLastName(lastName);
        entity.setAge(age);
        final UserEntity saved = repository.save(entity);
        log.info("Создан новый пользователь: {}", saved);
        return saved;
    }
}
