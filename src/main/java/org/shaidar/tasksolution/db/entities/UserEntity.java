package org.shaidar.tasksolution.db.entities;

import lombok.*;

import javax.persistence.Entity;

@Entity(name = "USERS")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString(callSuper = true)
public class UserEntity extends AbstractEntity {
    private String name;
    private String lastName;
    private Byte age;
}
